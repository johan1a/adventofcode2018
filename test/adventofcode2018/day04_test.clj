(ns adventofcode2018.day04-test
  (:require [clojure.test :refer :all]
            [adventofcode2018.day04 :refer :all]))

(deftest part-1
    (is (= (part1) 85296)))

(deftest part-2
    (is (= (part2) 58559)))

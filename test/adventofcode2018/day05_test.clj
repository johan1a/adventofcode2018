(ns adventofcode2018.day05-test
  (:require [clojure.test :refer :all]
            [adventofcode2018.day05 :refer :all]))

(deftest part-1
    (is (= (test-part1) 10)))

(deftest part-2
    (is (= (test-part2) 4)))

(ns adventofcode2018.day01-test
  (:require [clojure.test :refer :all]
            [adventofcode2018.day01 :refer :all]))

(deftest part-1
    (is (= (part1) 585)))

(deftest part-2
    (is (= (part2) 83173)))

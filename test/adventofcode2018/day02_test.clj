(ns adventofcode2018.day02-test
  (:require [clojure.test :refer :all]
            [adventofcode2018.day02 :refer :all]))

(deftest part-1
    (is (= (part1) 7221)))

(deftest part-2
    (is (= (part2) "mkcdflathzwsvjxrevymbdpoq")))

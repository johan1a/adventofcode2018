(ns adventofcode2018.day06-test
  (:require [clojure.test :refer :all]
            [adventofcode2018.day06 :refer :all]))

(deftest part-1-test
    (is (= (solve "test.txt") 17)))

(deftest part-1-test-1
    (is (= (solve "test1.txt") 15)))

(deftest part-1
    (is (= (solve "input.txt") 3907)))

(deftest part-2-test
    (is (= (solve-2 "test.txt" 32) 16)))

(deftest part-2
    (is (= (solve-2 "input.txt" 10000) 42036)))

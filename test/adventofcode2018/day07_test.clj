(ns adventofcode2018.day07-test
  (:require [clojure.test :refer :all]
            [adventofcode2018.day07 :refer :all]))

(deftest part-1-test
    (is (= (solve "test.txt") "CABDFE")))

(deftest part-1
    (is (= (solve "input.txt") "BFKEGNOVATIHXYZRMCJDLSUPWQ")))


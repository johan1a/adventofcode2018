(ns adventofcode2018.day03-test
  (:require [clojure.test :refer :all]
            [adventofcode2018.day03 :refer :all]))

(deftest part-1
    (is (= (part1) 114946)))

(deftest part-2
    (is (= (part2) 877)))

(ns adventofcode2018.day01
  (:gen-class)
  (:require [clojure.string :as str]))

(defn to-nbr
  [s]
  (. Integer parseInt s))

(defn get-input
  [file-name]
  (map to-nbr (str/split (slurp file-name) #"\n")))

(defn part1
  []
  (reduce + (get-input "resources/day01/input.txt")))

(defn first-duplicate
  ([input] (first-duplicate input #{0} 0 input))
  ([input seen sum nn]
   (if (empty? nn)
    (first-duplicate input seen sum input)
    (let [curr (first nn)
          new-sum (+ sum curr)]
      (if (contains? seen new-sum)
      new-sum
      (let [new-seen (conj seen new-sum)]
        (recur input new-seen new-sum (drop 1 nn))))))))

(defn part2
  []
  (first-duplicate (get-input "resources/day01/input.txt")))

(ns adventofcode2018.day05
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-polymer
  [filename]
  (str/trim (slurp (str "resources/day05/" filename))))

(defn can-react?
  [polymer index0 index1]
  (if (or (< index0 0) (> index1 (- (count polymer) 1))) false
      (let [letter0 (nth polymer index0)
            letter1 (nth polymer index1)]
        (and (not (= letter0 letter1))
             (= (str/lower-case letter0) (str/lower-case letter1))))))

(defn react
  [state index0 index1]
  (let [polymer (:polymer state)
        letter0 (nth polymer index0)
        letter1 (nth polymer index1)
        new-state (assoc state :polymer (concat (take index0 polymer) (drop (inc index1) polymer)))]
    (assoc new-state :index index0)))

(defn final-length
  [state]
  (let [polymer (:polymer state)
        index (:index state)]
    (if (= (count polymer) index)
      (count polymer)
      (cond
        (can-react? polymer (- index 1) index) (recur (react state (- index 1) index))
        (can-react? polymer index (+ index 1)) (recur (react state index (+ index 1)))
        :else (recur (assoc state :index (+ index 1)))))))

(defn solve
  [polymer]
  (final-length (assoc {:polymer polymer} :index 0)))

(defn test-part1
  []
  (solve (get-polymer "test_input1.txt")))

(defn part1
  []
  (solve (get-polymer "input.txt")))

(defn alphabet [] (vec "abcdefghijklmnopqrstuvwxyz"))

(defn filter-polymer
  [polymer letter]
  (let [filtered (filter #(not (= (str/lower-case %) (str letter))) polymer)]
    (assoc {:polymer filtered} :index 0)))

(defn solve2
  [polymer]
  (let [polymers (pmap #(filter-polymer polymer %) (alphabet))]
    (first (sort (pmap final-length polymers)))))

(defn test-part2
  []
  (solve2 (get-polymer "test_input1.txt")))

(defn part2
  []
  (solve2 (get-polymer "input.txt")))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

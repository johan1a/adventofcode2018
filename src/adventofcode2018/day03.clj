(ns adventofcode2018.day03
  (:gen-class)
  (require [clojure.string :as str]))

(defn parse-rect
  [line]
  (let [matches (map #(Integer/parseInt %) (drop 1 (re-matches #"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)" line)))
        id  (nth matches 0)
        x (nth matches 1)
        y (nth matches 2)
        width (nth matches 3)
        height (nth matches 4)]
    {:id id :x x :y y :width width :height height}))

(defn get-rects
  [filename]
  (map parse-rect (str/split (slurp (str "resources/day03/" filename)) #"\n")))

(defn make-coord
  [x y]
  {:x x :y y})

(defn make-coords
  [x y-range]
  (map (fn [y] (make-coord x y)) y-range))

(defn make-rect-coords1
  ([x-range y-range] (make-rect-coords1 [] x-range y-range))
  ([coords unchecked-x y-range]
   (if (empty? unchecked-x)
     coords
     (let [x (first unchecked-x)
           new-coords (concat coords (make-coords x y-range))]
       (recur new-coords (drop 1 unchecked-x) y-range)))))

(defn make-rect-coords
  [rect]
  (let [x-range (range (:x rect) (+ (:x rect) (:width rect)))
        y-range (range (:y rect) (+ (:y rect) (:height rect)))]
    (make-rect-coords1 x-range y-range)))

(defn increase-count
  [k]
  (if k (+ k 1) 1))

(defn mark-coords
  [marked coords]
  (if (empty? coords)
    marked
    (recur (update marked (first coords) increase-count) (drop 1 coords))))

(defn mark-rect
  [marked rect]
  (let [coords (make-rect-coords rect)]
    (mark-coords marked coords)))

(defn mark-inches
  ([rects] (mark-inches {} rects))
  ([marked rects]
   (if (empty? rects)
     marked
     (recur (mark-rect marked (first rects)) (drop 1 rects)))))

(defn nbr-multiple-claims
  [inputfile]
  (count (filter #(> (second %) 1) (mark-inches (get-rects inputfile)))))

(defn overlapping?
  [marks rect]
  (not (every? #(> 2 (get marks %)) (make-rect-coords rect))))

(defn find-non-overlapping
  [inputfile]
  (let [rects (get-rects inputfile)
        marks (mark-inches rects)]
    (:id (first (filter #(not (overlapping? marks %)) rects)))))

(defn test-part1
  []
  (nbr-multiple-claims "test_input1.txt"))

(defn part1
  []
  (nbr-multiple-claims "input1.txt"))

(defn -main
  [& args]
  (println "Hello, World!"))

(defn part2
  []
  (find-non-overlapping "input1.txt"))

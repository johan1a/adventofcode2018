(ns adventofcode2018.day04
  (:gen-class)
  (:require [clojure.string :as str]
            [java-time :as ct]))

(def l0 "[1518-11-01 00:00] Guard #10 begins shift")
(def l1 "[1518-11-01 00:05] falls asleep")

(def regex-begin #"\[(\d+)-(\d+)-(\d+) (\d+):(\d+)\] Guard #(\d+) begins shift")
(def regex-asleep #"\[(\d+)-(\d+)-(\d+) (\d+):(\d+)\] falls asleep")
(def regex-wakeup #"\[(\d+)-(\d+)-(\d+) (\d+):(\d+)\] wakes up")
(def regex-date #"\[(\d+)-(\d+)-(\d+) (\d+):(\d+)\].*")

(defn parse-begin
  [line]
  (let [numbers (map #(Integer/parseInt %) (drop 1 (re-matches regex-begin line)))
        year (nth numbers 0)
        month (nth numbers 1)
        day (nth numbers 2)
        hour (nth numbers 3)
        minute (nth numbers 4)]
    {:datetime (ct/local-date-time year month day hour minute)
     :guard-id (nth numbers 5)
     :type "begin"}))

(defn parse-asleep
  [line]
  (let [numbers (map #(Integer/parseInt %) (drop 1 (re-matches regex-asleep line)))
        year (nth numbers 0)
        month (nth numbers 1)
        day (nth numbers 2)
        hour (nth numbers 3)
        minute (nth numbers 4)]
    {:datetime (ct/local-date-time year month day hour minute)
     :type "asleep"}))

(defn parse-wakeup
  [line]
  (let [numbers (map #(Integer/parseInt %) (drop 1 (re-matches regex-wakeup line)))
        year (nth numbers 0)
        month (nth numbers 1)
        day (nth numbers 2)
        hour (nth numbers 3)
        minute (nth numbers 4)]
    {:datetime (ct/local-date-time year month day hour minute)
     :type "wakeup"}))

(def empty-state
  {:curr nil
   :events {}})

(defn is-begin?
  [line]
  (.contains line "begins"))

(defn is-sleep?
  [line]
  (.contains line "asleep"))

(defn is-wakeup?
  [line]
  (.contains line "wakes"))

(defn add-to-events
  [all-events id event]
  (update all-events id (fn [events]
                          (if events
                            (conj events event)
                            [event]))))

(defn eval-begin
  [state event]
  (let [id (:guard-id event)]
    (update (assoc state :curr id) :events #(add-to-events % id event))))

(defn eval-event
  [state event]
  (let [id (:curr state)]
    (update state :events #(add-to-events % id event))))

(defn eval-line
  [state line]
  (cond (is-begin? line) (eval-begin state (parse-begin line))
        (is-sleep? line) (eval-event state (parse-asleep line))
        (is-wakeup? line) (eval-event state (parse-wakeup line))))

(defn parse-lines
  ([lines] (parse-lines empty-state lines))
  ([state lines]
   (if (empty? lines)
     state
     (recur (eval-line state (first lines)) (rest lines)))))

(defn to-date
  [line]
  (apply ct/local-date-time (map #(Integer/parseInt %) (rest (re-matches regex-date line)))))

(defn sort-by-date
  [lines]
  (sort-by to-date lines))

(defn parse-input
  [filename]
  (let [lines (sort-by-date (str/split (slurp (str "resources/day04/" filename)) #"\n"))]
    (parse-lines lines)))

(defn inc-frequencies
  [sleep-state start end]
  (let [start-minute (ct/as start :minute-of-hour)
        end-minute (ct/as end :minute-of-hour)]
    (if (= start-minute end-minute)
      sleep-state
      (recur (update-in sleep-state [:frequencies start-minute] inc) (ct/plus start (ct/minutes 1)) end))))

(defn empty-frequencies [] (into {} (vec (map #(do [% 0]) (range 0 60)))))

(defn empty-sleep-state [] {:frequencies (empty-frequencies) :sleep-minutes 0})

(defn add-sleep-time
  [sleep-state sleep-event wakeup-event]
  (let [sleep-time (:datetime sleep-event)
        wakeup-time (:datetime wakeup-event)
        sleep-state-updated-total (update sleep-state :sleep-minutes #(+ (ct/time-between sleep-time wakeup-time :minutes) %))]
    (inc-frequencies sleep-state-updated-total sleep-time  wakeup-time)))

(defn count-guard-sleep
  ([events sleep-state]
   (if (or (< (count events) 2) (not events)) sleep-state
       (let [event (first events)
             next-event (nth events 1)
             type1 (:type event)]
         (cond (= type1 "begin") (count-guard-sleep (rest events) sleep-state)
               (= type1 "asleep") (count-guard-sleep (drop 2 events) (add-sleep-time sleep-state event next-event))
               (= type1 "wakeup") (throw (Exception. "wtf")))))))

(defn call-count-guard-sleep
  [state guard-id]
  (let [guard-events (get (:events state) guard-id)]
    (count-guard-sleep guard-events (assoc (empty-sleep-state) :guard-id guard-id))))

(defn count-sleep
  [state]
  (let [guard-ids (keys (:events state))]
    (map #(call-count-guard-sleep state %)
         guard-ids)))

(defn most-slept-minute
  [guard]
  (first (last (sort-by second (:frequencies guard)))))

(defn highest-frequency
  [guard]
  (second (last (sort-by second (:frequencies guard)))))

(defn find-most-sleepy
  [filename]
  (let [guard (last (sort-by :sleep-minutes (count-sleep (parse-input filename))))
        minute            (most-slept-minute guard)]
    (* (:guard-id guard) minute)))

(defn find-most-frequent
  [filename]
  (let [guard (last (sort-by highest-frequency (count-sleep (parse-input filename))))
        minute            (most-slept-minute guard)]
    (* (:guard-id guard) minute)))

(defn test-part1
  []
  (find-most-sleepy "test_input1.txt"))

(defn part1
  []
  (find-most-sleepy "input1.txt"))

(defn test-part2
  []
  (find-most-frequent "test_input1.txt"))

(defn part2
  []
  (find-most-frequent "input1.txt"))

; (map #(do [(:guard-id % ) (most-slept-minute %)]) (sort-by most-slept-minute (count-sleep (parse-input "test_input1.txt"))))

(defn -main
  [& args]
  (println "Hello, World!"))

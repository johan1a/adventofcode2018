(ns adventofcode2018.day06
  (:gen-class)
  (:require [clojure.string :as str]
            [clojure.pprint :refer [pprint]]))

(defn to-int-coord
  [str-coord]
  (map #(Integer/parseInt %) str-coord))

(defn get-input
  [filename]
  (let [data (str/trim (slurp (str "resources/day06/" filename)))
        lines (str/split data #"\n")
        str-coords (map #(str/split % #", ") lines)
        coords (map to-int-coord str-coords)]
    (map vec coords)))

(defn get-limits
  ([coords]
   (let [min-x (first (apply min-key first coords))
         max-x (first (apply max-key first coords))
         min-y (second (apply min-key second coords))
         max-y (second (apply max-key second coords))
         limits [min-x max-x min-y max-y]]
     limits)))

(defn inside-limits
  [[min-x max-x min-y max-y] [x y]]
  (and
   (>= x min-x)
   (<= x max-x)
   (>= y min-y)
   (<= y max-y)))

(defn add-coord
  [[x0 y0] [x1 y1]]
  [(+ x0 x1) (+ y0 y1)])

(defn get-neighbors
  ([dist limits coord] (get-neighbors dist limits coord coord))
  ([dist limits coord origin]
   (let [all-neighbors (map #(add-coord coord %) [[0 1] [0 -1] [1 0] [-1 0]])
         valid-neighbors (filter #(inside-limits limits %) all-neighbors)
         with-origin (map (fn [n] [dist n (vec origin)]) valid-neighbors)]
     with-origin)))

(defn compare-to-check
  ([a b] (compare a b)))

(defn get-min-map
  ([limits coords]
   (let [dists (into {} (map (fn [c] [(vec c) [0 (vec c)]]) coords))
         all-neighbors (mapcat #(get-neighbors 1 limits %) coords)
         to-check (into (sorted-set-by compare-to-check) all-neighbors)]
     (get-min-map limits dists to-check)))
  ([limits dists to-check]
   (if (empty? to-check)
     dists
     (let [[dist coord origin] (first to-check)
           [current-dist cur-origin] (get dists coord)
           neighbors (get-neighbors (+ dist 1) limits coord origin)
           with-neighbors (into to-check neighbors)]
       (if (or (= nil current-dist) (> current-dist dist))
         (let [next-dists (assoc dists coord [dist origin])
               next-to-check (disj with-neighbors [dist coord origin])]
           (recur limits next-dists next-to-check))
         (if
          (= current-dist dist)
           (let [next-dists (assoc dists coord [dist nil])
                 next-to-check (disj with-neighbors [dist coord origin])]
             (recur limits next-dists next-to-check))
           (recur limits dists (disj to-check [dist coord origin]))))))))

(defn get-ids
  [coords]
  (zipmap coords (range 0 (count coords))))

(defn print-grid
  [[x0 x1 y0 y1] coords dists]
  (let [ids (get-ids coords)]
    (doseq [y (range 0 (+ y1 1))]
      (doseq [x (range 0 (+ x1 1))]
        (let [[dist origin] (get dists (seq [x y]))
              id (get ids (seq origin))]
          (if-not (= nil id)
            (print (mod id 10))
            (print "."))))
      (println))))

(defn get-origins
  [dists border]
  (distinct (map #(second (get dists %)) border)))

(defn get-infinite-coords
  [dists [min-x max-x min-y max-y] coords]
  (let [border-x (range min-x (+ max-x 1))
        border-y (range min-y (+ max-y 1))
        top-border (map (fn [x] [x min-y]) border-x)
        bottom-border (map (fn [x] [x max-y]) border-x)
        left-border (map (fn [y] [min-x y]) border-y)
        right-border (map (fn [y] [max-x y]) border-y)
        inf-coords (distinct (concat (get-origins dists top-border)
                                     (get-origins dists bottom-border)
                                     (get-origins dists left-border)
                                     (get-origins dists right-border)))]
    inf-coords))

(defn get-largest-size
  [dists eligible-coords]
  (let [all-freqs (frequencies (map second (vals dists)))
        freqs (filter #(.contains eligible-coords (first %)) all-freqs)
        largest (apply max (map second freqs))]
    largest))

(defn solve
  [filename]
  (let [coords (get-input filename)
        limits (get-limits coords)
        dists (get-min-map limits coords)
        infinite-coords (get-infinite-coords dists limits coords)
        eligible (filter #(not (.contains infinite-coords %)) coords)
        largest-size (get-largest-size dists eligible)]
    largest-size))

(defn distance-between
  [[x0 y0] [x1 y1]]
  (+ (Math/abs (- x1 x0)) (Math/abs (- y1 y0))))

(defn distance-sum-lt
  ([max-dist targets source] (distance-sum-lt max-dist targets source 0))
  ([max-dist [target & targets] source sum]
   (if (= nil target)
     (< sum max-dist)
     (if (>= sum max-dist)
       false
       (let [distance (distance-between source target)
             distance-sum (+ sum distance)]
         (recur max-dist targets source distance-sum))))))

(defn find-region
  ([coords max-dist] (let [initial-coords (filter #(distance-sum-lt max-dist coords %) coords)]
                       (find-region coords max-dist initial-coords #{} 0)))
  ([coords max-dist [coord & to-check] visited sum]
   (if (= coord nil)
     sum
     (if
      (.contains visited coord)
       (recur coords max-dist to-check visited sum)
       (let [neighbors (map #(add-coord coord %) [[0 1] [0 -1] [1 0] [-1 0]])
             unseen-neighbors (filter #(not (.contains visited %)) neighbors)
             close-neighbors (filter #(distance-sum-lt max-dist coords %) unseen-neighbors)]
         ;(when (= 0 (mod (first coord) 100)) (println coord))
         (recur coords max-dist (apply conj to-check close-neighbors) (conj visited coord) (+ sum 1)))))))

(defn solve-2
  [filename dist]
  (let [coords (get-input filename)]
    (find-region coords dist)))

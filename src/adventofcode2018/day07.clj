(ns adventofcode2018.day07
  (:gen-class)
  (:require [clojure.string :as str]
            [clojure.pprint :refer [pprint]]))
(defn get-rule
  [line]
  (let [rule (-> line
                 (str/replace #"Step " "")
                 (str/replace #" can begin." "")
                 (str/split #" must be finished before step "))]
    {:first (first rule) :second (second rule)}))

(defn get-input
  [filename]
  (let [data (str/trim (slurp (str "resources/day07/" filename)))
        lines (str/split data #"\n")
        plain-rules (map get-rule lines)
        all-steps (apply hash-set (flatten (map (fn [r] [(:first r) (:second r)]) plain-rules)))
        second-rules (group-by :second plain-rules)
        dependencies (zipmap (keys second-rules) (map #(map :first %) (vals second-rules)))
        first-rules (group-by :first plain-rules)
        inverse-dependencies (zipmap (keys first-rules) (map #(map :second %) (vals first-rules)))
        available (sort (filter #(not (contains? dependencies %)) all-steps))]
    [dependencies inverse-dependencies available]))

(defn remove-dependencies
  [dependencies [affected & aa] step steps]
  (if (= nil affected)
    [dependencies (sort steps)]
    (let [next-dependencies (update-in dependencies [affected] (fn [d] (remove #(= % step) d)))
          next-steps (if (empty? (get next-dependencies affected)) (conj steps affected) steps)]
      (recur next-dependencies aa step next-steps))))

(defn find-order
  [dependencies inverse [step & steps] order]
  (if (= nil step) (str/join order)
      (let [affected (get inverse step)
            [next-dependencies next-steps] (remove-dependencies dependencies affected step steps)]

        (recur next-dependencies inverse next-steps (conj order step)))))

(defn solve
  [filename]
  (let [[dependencies inverse-dependencies available] (get-input filename)]
    (find-order dependencies inverse-dependencies available [])))

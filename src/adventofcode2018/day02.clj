(ns adventofcode2018.day02
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-input
  [filename]
  (str/split (slurp (str "resources/day02/" filename)) #"\n"))

(defn check-duplicates
  ([n line] (check-duplicates n line line))
  ([n unchecked line]
   (if (empty? unchecked)
     0
     (let [curr (first unchecked)
           nbr-duplicates (count (filter #(= curr %) line))]
       (if (= nbr-duplicates n)
         1
         (recur n (drop 1 unchecked) line))))))

(defn count-letters
  ([input] (count-letters 0 0 input))
  ([twos threes input]
   (if (empty? input)
     (* twos threes)
     (let [two-score (check-duplicates 2 (first input))
           three-score (check-duplicates 3 (first input))]
       (recur (+ twos two-score) (+ threes three-score) (drop 1 input))))))

(defn letter-distance
  [letter-a letter-b]
  (if (= letter-a letter-b) 1 0))

(defn distance
  [line-a line-b]
  (let [sum (reduce + (map letter-distance line-a line-b))]
    (- (count line-a) sum)))

(defn correct-pair?
  [a b]
  (= (distance a b) 1))

(defn common-letters
  ([id-a id-b] (common-letters '() id-a id-b))
  ([result id-a id-b]
   (if (empty? id-a)
     (apply str (reverse result))
     (let [letter-a (first id-a)
           letter-b (first id-b)]
     (if (= letter-a letter-b)
       (recur (conj result letter-a) (drop 1 id-a) (drop 1 id-b))
       (recur result (drop 1 id-a) (drop 1 id-b)))))))

(defn compare-letters
  ([input] (compare-letters input input))
  ([unchecked line]
   (let [curr (first unchecked)
         matching (first (filter #(correct-pair? curr %) (drop 1 unchecked)))]
     (if matching
       (common-letters curr matching)
       (recur (drop 1 unchecked) line)))))

(defn -main
  [& args]
  (println "Hello, World!"))

(defn test-part1
  []
  (count-letters (get-input "test_input1.txt")))

(defn part1
  []
  (count-letters (get-input "input.txt")))

(defn test-part2
  []
  (compare-letters (get-input "test_input2.txt")))

(defn part2
  []
  (compare-letters (get-input "input.txt")))
